module TokenAuth
  extend ActiveSupport::Concern

  protected

  def current_user
    User.first
  end
end
