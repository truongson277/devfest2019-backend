class Position < ApplicationRecord
  belongs_to :positionable, polymorphic: true
  reverse_geocoded_by :latitude, :longitude

  scope :_positionable_type, ->(type) {where(positionable_type: type)}

  class << self
    def nearest(location)
      Position.near(
        location,
        Settings.nearest_distance,
        units: Settings.nearest_unit
      )
    end
  end
end
