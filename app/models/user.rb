class User < ApplicationRecord
  has_many :reports
  has_many :owner_events, class_name: 'Event', foreign_key: :user_id
  has_many :event_joiners
  has_many :join_events, through: :event_joiners, source: :event
end
