class GarbageSerializer
  include FastJsonapi::ObjectSerializer
  attributes :qrcode, :name, :status, :image, :created_at, :updated_at
end
