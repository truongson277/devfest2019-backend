class Garbages::ShowSerializer
  include FastJsonapi::ObjectSerializer
  attributes :address, :latitude, :longitude
end
