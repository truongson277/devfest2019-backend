class Garbages::IndexSerializer < GarbageSerializer
  attributes :latitude, :longitude, :address
end
