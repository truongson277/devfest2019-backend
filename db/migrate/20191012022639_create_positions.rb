class CreatePositions < ActiveRecord::Migration[5.2]
  def change
    create_table :positions do |t|
      t.references :positionable, polymorphic: true
      t.float :latitude
      t.float :longitude
      t.text :address

      t.timestamps
    end
  end
end
