Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      post 'reports', to: 'reports#create'
      get 'reports/histories', to: 'reports#index'
      get 'report/:id', to: 'reports#show'
      get 'garbages/nearest', to: 'garbages#index'
      get 'garbages/show/:qrcode', to: 'garbages#show'
      post 'events', to: 'events#create'
      get 'events/current_week', to: 'events#index_current_week'
      get 'events/incomming', to: 'events#incomming'
    end
  end
end
